<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client as GuzzleClient;
use http\Cookie;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    private $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->client = new GuzzleClient(['base_uri' => 'https://smshub.org/stubs/']);
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function checkAuth(Request $request)
    {
        $this->validate($request, [
            'api_key' => 'string|max:255',
        ]);

        $params = [
            'api_key' => $request->api_key,
            'action' => 'getBalance'
        ];

        $result = $this->client->request('GET', 'handler_api.php', ['query' => $params]);
        $response_string = (string) $result->getBody();

        $data = explode(':', $response_string);

        if ($data[0] == 'ACCESS_BALANCE') {
//            Cookie::queue('api_key', $request->api_key);

            return ok($data);
        }

        return unauthenticated('bad key')->cookie(
            'api_key', $request->api_key, 1440
        );

    }
}
